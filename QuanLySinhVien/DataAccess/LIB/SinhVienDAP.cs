﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.LIB
{
    public class SinhVienDAP
    {
        QuanLySinhVienEntities dbContext;
        public SinhVienDAP(QuanLySinhVienEntities _dbContext)
        {
            dbContext = _dbContext;
        }

        public List<SinhVienEntity> getPaged(int pageNum, int pageSize, string strTenSinhVien)
        {
            int excludedRows = (pageNum - 1) * pageSize;
            strTenSinhVien = strTenSinhVien.Trim().ToLower();
            var query = (from obj in dbContext.SinhViens
                         where obj.TEN.Contains(strTenSinhVien)
                         select new SinhVienEntity
                         {
                             ID = obj.ID,
                             MSV = obj.MSV,
                             TEN = obj.TEN,
                             TenLop = obj.LopHoc.TEN
                         });
            return query.Take(pageSize).Skip(excludedRows).ToList();
        }

        public List<SinhVienEntity> getAllSinhVien()
        {
            var query = (from obj in dbContext.SinhViens
                         select new SinhVienEntity
                         {
                             ID = obj.ID,
                             MSV = obj.MSV,
                             TEN = obj.TEN,
                             TenLop = obj.LopHoc.TEN
                         });
            return query.OrderBy(p => p.TEN).ToList();
        }

        public SinhVien getByID(int Id)
        {
            return dbContext.SinhViens.Where(p => p.ID == Id).FirstOrDefault();
        }

        public int Add(SinhVien objSinhVien)
        {
            dbContext.SinhViens.Add(objSinhVien);
            return dbContext.SaveChanges();
        }

        public int Save()
        {
            return dbContext.SaveChanges();
        }

        public int Delete(int Id)
        {
            SinhVien objSinhVien = getByID(Id);
            dbContext.SinhViens.Remove(objSinhVien);
            return dbContext.SaveChanges();
        }
    }
}
