﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.LIB
{
    class LopHocDAP
    {
        QuanLySinhVienEntities dbContext;
        public LopHocDAP(QuanLySinhVienEntities _dbContext)
        {
            dbContext = _dbContext;
        }

        public List<LopHocEntity> getPaged(int pageNum, int pageSize, string strTenLopHoc)
        {
            int excludedRows = (pageNum - 1) * pageSize;
            strTenLopHoc = strTenLopHoc.Trim().ToLower();
            var query = (from obj in dbContext.LopHocs
                         where obj.TEN.Contains(strTenLopHoc)
                         select new LopHocEntity
                         {
                             ID = obj.ID,
                             ML = obj.ML,
                             TEN = obj.TEN,
                             TenKhoa = obj.Khoa.TEN
                         });
            return query.Take(pageSize).Skip(excludedRows).ToList();
        }

        public List<LopHocEntity> getAllLopHoc()
        {
            var query = (from obj in dbContext.LopHocs
                         select new LopHocEntity
                         {
                             ID = obj.ID,
                             ML = obj.ML,
                             TEN = obj.TEN,
                             TenKhoa = obj.Khoa.TEN
                         });
            return query.OrderBy(p => p.TEN).ToList();
        }

        public LopHoc getByID(int Id)
        {
            return dbContext.LopHocs.Where(p => p.ID == Id).FirstOrDefault();
        }

        public int Add(LopHoc objLopHoc)
        {
            dbContext.LopHocs.Add(objLopHoc);
            return dbContext.SaveChanges();
        }

        public int Save()
        {
            return dbContext.SaveChanges();
        }

        public int Delete(int Id)
        {
            LopHoc objLopHoc = getByID(Id);
            dbContext.LopHocs.Remove(objLopHoc);
            return dbContext.SaveChanges();
        }
    }
}
