﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.LIB
{
    class KhoaDAP
    {
        QuanLySinhVienEntities dbContext;
        public KhoaDAP(QuanLySinhVienEntities _dbContext)
        {
            dbContext = _dbContext;
        }

        public List<KhoaEntity> getPaged(int pageNum, int pageSize, string strTenKhoa)
        {
            int excludedRows = (pageNum - 1) * pageSize;
            strTenKhoa = strTenKhoa.Trim().ToLower();
            var query = (from obj in dbContext.Khoas
                         where obj.TEN.Contains(strTenKhoa)
                         select new KhoaEntity
                         {
                             ID = obj.ID,
                             TEN = obj.TEN,
                         });
            return query.Take(pageSize).Skip(excludedRows).ToList();
        }

        public List<KhoaEntity> getAllKhoa()
        {
            var query = (from obj in dbContext.Khoas
                         select new KhoaEntity
                         {
                             ID = obj.ID,
                             TEN = obj.TEN,
                         });
            return query.OrderBy(p => p.TEN).ToList();
        }

        public Khoa getByID(int Id)
        {
            return dbContext.Khoas.Where(p => p.ID == Id).FirstOrDefault();
        }

        public int Add(Khoa objKhoa)
        {
            dbContext.Khoas.Add(objKhoa);
            return dbContext.SaveChanges();
        }

        public int Save()
        {
            return dbContext.SaveChanges();
        }

        public int Delete(int Id)
        {
            Khoa objKhoa = getByID(Id);
            dbContext.Khoas.Remove(objKhoa);
            return dbContext.SaveChanges();
        }
    }
}
