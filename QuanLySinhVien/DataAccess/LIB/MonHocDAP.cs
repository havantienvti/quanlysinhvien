﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.LIB
{
    class MonHocDAP
    {
        QuanLySinhVienEntities dbContext;
        public MonHocDAP(QuanLySinhVienEntities _dbContext)
        {
            dbContext = _dbContext;
        }

        public List<MonHocEntity> getPaged(int pageNum, int pageSize, string strTenMonHoc)
        {
            int excludedRows = (pageNum - 1) * pageSize;
            strTenMonHoc = strTenMonHoc.Trim().ToLower();
            var query = (from obj in dbContext.MonHocs
                         where obj.TEN.Contains(strTenMonHoc)
                         select new MonHocEntity
                         {
                             ID = obj.ID,
                             MMH = obj.MMH,
                             TEN = obj.TEN
                         });
            return query.Take(pageSize).Skip(excludedRows).ToList();
        }

        public List<MonHocEntity> getAllMonHoc()
        {
            var query = (from obj in dbContext.MonHocs
                         select new MonHocEntity
                         {
                             ID = obj.ID,
                             MMH = obj.MMH,
                             TEN = obj.TEN
                         });
            return query.OrderBy(p => p.TEN).ToList();
        }

        public MonHoc getByID(int Id)
        {
            return dbContext.MonHocs.Where(p => p.ID == Id).FirstOrDefault();
        }

        public int Add(MonHoc objMonHoc)
        {
            dbContext.MonHocs.Add(objMonHoc);
            return dbContext.SaveChanges();
        }

        public int Save()
        {
            return dbContext.SaveChanges();
        }

        public int Delete(int Id)
        {
            MonHoc objMonHoc = getByID(Id);
            dbContext.MonHocs.Remove(objMonHoc);
            return dbContext.SaveChanges();
        }
    }
}
