﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.LIB
{
    class DangKyHocDAP
    {
        QuanLySinhVienEntities dbContext;
        public DangKyHocDAP(QuanLySinhVienEntities _dbContext)
        {
            dbContext = _dbContext;
        }

        public List<DangKyHocEntity> getPaged(int pageNum, int pageSize, string ID)
        {
            int excludedRows = (pageNum - 1) * pageSize;
            var query = (from obj in dbContext.DangKyHocs
                         where
                         select new DangKyHocEntity
                         {
                             ID = obj.ID,
                             NAMHOC = obj.NAMHOC,
                             KYHOC = obj.KYHOC,
                             TenSinhVien = obj.SinhVien.TEN,
                             TenMonHoc = obj.MonHoc.TEN,
                             TenGiaoVien = obj.GiaoVien.TEN,
                             NGAYDANGKY = obj.NGAYDANGKY
                         }) ;
            return query.Take(pageSize).Skip(excludedRows).ToList();
        }

        public List<DangKyHocEntity> getAllDangKyHoc()
        {
            var query = (from obj in dbContext.DangKyHocs
                         select new DangKyHocEntity
                         {
                             ID = obj.ID,
                             NAMHOC = obj.NAMHOC,
                             KYHOC = obj.KYHOC,
                             TenSinhVien = obj.SinhVien.TEN,
                             TenMonHoc = obj.MonHoc.TEN,
                             TenGiaoVien = obj.GiaoVien.TEN,
                             NGAYDANGKY = obj.NGAYDANGKY
                         });
            return query.OrderBy(p => p.ID).ToList();
        }

        public DangKyHoc getByID(int Id)
        {
            return dbContext.DangKyHocs.Where(p => p.ID == Id).FirstOrDefault();
        }

        public int Add(DangKyHoc objDangKyHoc)
        {
            dbContext.DangKyHocs.Add(objDangKyHoc);
            return dbContext.SaveChanges();
        }

        public int Save()
        {
            return dbContext.SaveChanges();
        }

        public int Delete(int Id)
        {
            DangKyHoc objDangKyHoc = getByID(Id);
            dbContext.DangKyHocs.Remove(objDangKyHoc);
            return dbContext.SaveChanges();
        }
    }
}
