﻿//chạy khi bắt đầu
$(document).ready(() => {
    $("#create-sv").click(function () {
        $.post('DetailForm.aspx', {"do": "add"},function (data) {
            $('#jdialog').html(data);
            $('#jdialog').dialog({
                title:"Thêm sinh viên mới",
                autoOpen: false,
                height: 400,
                width: 550,
                modal: true
            }).dialog('open');
            $("#btnUpdate").hide();
        })
    });

    print();
})

// Data fix cứng
var MyArr = [
    { msv: "11174694", name: "Hà Văn Tiến", class: "Công nghệ thông tin 59B", khoa: "CNTT", gt: "Nam", ns: "1999-02-19", qq: "Thái Bình" },
    { msv: "11174695", name: "Bùi Đức Lương", class: "Kiểm toán 59A", khoa: "CNTT", gt: "Nam", ns: "1999-10-31", qq: "Ninh Bình" },
    { msv: "11174696", name: "Nguyễn Văn Trung", class: "Bất động sản 59A", khoa: "BĐS", gt: "Nam", ns: "1999-06-05", qq: "Nam Định" }
];

//print table
function print() {
    const listUser = document.getElementById('listUser');
    let s = '';
    MyArr.forEach((ele, i) => {
        s += `
        <tr>
            <th scope="row">${i + 1} </th>
            <td> ${MyArr[i].msv} </td>
            <td> ${MyArr[i].name} </td>
            <td> ${MyArr[i].class} </td>
            <td> ${MyArr[i].khoa} </td>
            <td> ${MyArr[i].gt} </td>
            <td> ${MyArr[i].ns} </td>
            <td> ${MyArr[i].qq} </td>
            <td><span class="btn btn-sm btn-warning" onclick="EditUser(${i})">Sửa</span>
            <span class="btn btn-sm btn-danger" onclick="deleteUser(${i})">Xóa</span></td>
        </tr>`
    })
    listUser.innerHTML = s;
}

// Delete
function deleteUser(id) {
    MyArr.splice(id, 1);
    print()
}

// Edit
function EditUser(id) {
    const data = MyArr[id]
    $.post('DetailForm.aspx', { "do": "edit" }, function (data1) {
        $('#jdialog').html(data1);
        $('#jdialog').dialog({
            title: "Sửa thông tin sinh viên",
            autoOpen: false,
            height: 400,
            width: 550,
            modal: true
        }).dialog('open');
        $("#btnAdd").hide();
        $('#txtMsv').val(data.msv);
        $('#txtName').val(data.name);
        $('#txtClass').val(data.class);
        $('#txtKhoa').val(data.khoa);
        $('#cboGioiTinh').val(data.gt);
        $('#dateNgaySinh').val(data.ns);
        $('#txtQueQuan').val(data.qq);
        localStorage.setItem("idEdit", id);   
    })
}

// Search
$(document).ready(function () {
    $("#myInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#listUser tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
