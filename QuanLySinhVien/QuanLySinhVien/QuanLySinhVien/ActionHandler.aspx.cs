﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using DataAccess.LIB;

namespace QuanLySinhVien.QuanLySinhVien
{
    public partial class ActionHandler : System.Web.UI.Page
    {
        QuanLySinhVienEntities dbContext;
        
        protected void Page_Load(object sender, EventArgs e)
        {

            dbContext = new QuanLySinhVienEntities();
            SinhVienDAP resSV = new SinhVienDAP(dbContext);
            List <SinhVienEntity> lstsv= resSV.getAllSinhVien();
        }
    }
}