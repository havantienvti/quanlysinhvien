﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetailForm.aspx.cs" Inherits="QuanLySinhVien.QuanLySinhVien.DetailForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function addSV() {
            var sv = {}
            sv.msv = $('#txtMsv').val(),
                sv.name = $('#txtName').val(),
                sv.class = $('#txtClass').val(),
                sv.khoa = $('#txtKhoa').val(),
                sv.gt = $('#cboGioiTinh').val(),
                sv.ns = $('#dateNgaySinh').val(),
                sv.qq = $('#txtQueQuan').val(),
            MyArr.push(sv);
            $("#jdialog").dialog().dialog('close');
            print();
        }

        function updateSV() {
            console.log("aaaaaa")
            var sv = {}
            sv.msv = $('#txtMsv').val(),
                sv.name = $('#txtName').val(),
                sv.class = $('#txtClass').val(),
                sv.khoa = $('#txtKhoa').val(),
                sv.gt = $('#cboGioiTinh').val(),
                sv.ns = $('#dateNgaySinh').val(),
                sv.qq = $('#txtQueQuan').val(),
                id = localStorage.getItem("idEdit")
            MyArr[id] = sv;
            $("#jdialog").dialog().dialog('close');
            print();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="txtMsv" class="col-form-label">Mã SV:</label>
                        <input type="text" class="form-control" id="txtMsv">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="txtName" class="col-form-label">Họ và tên:</label>
                        <input type="text" class="form-control" id="txtName">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="txtClass" class="col-form-label">Lớp:</label>
                        <input type="text" class="form-control" id="txtClass">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="txtKhoa" class="col-form-label">Khoa:</label>
                        <input type="text" class="form-control" id="txtKhoa">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="cboGioiTinh" class="col-form-label">Giới tính:</label>
                        <select id="cboGioiTinh">
                            <option value="Nam">Nam</option>
                            <option value="Nữ">Nữ</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="dateNgaySinh" class="col-form-label">Ngày Sinh:</label>
                        <input type="date" id="dateNgaySinh" name="dateNgaySinh">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="txtQueQuan" class="col-form-label">Quê quán:</label>
                        <input type="text" class="form-control" id="txtQueQuan">
                    </div>
                </div>
            
            <div class="modal-footer">
                <button type="button" id="btnAdd" onclick="addSV()" class="btn btn-success">Thêm</button>
                <button type="button" id="btnUpdate" onclick="updateSV()" class="btn btn-success">Lưu</button>
            </div>
      
    </form>
</body>
</html>
