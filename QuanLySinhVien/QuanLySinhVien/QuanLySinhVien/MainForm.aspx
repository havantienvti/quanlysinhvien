﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true" CodeBehind="MainForm.aspx.cs" Inherits="QuanLySinhVien.QuanLySinhVien.MainForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head1" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentplace2" runat="server">
    <div>
        <div id="jdialog"></div>
        <p class="main-top">Danh sách Sinh viên</p>
        <div class="main-top1">
            <div class="col-sm-2">
                <p class="btn btn-success" id="create-sv">
                    <span><i class="fas fa-plus"></i></span>
                    <span style="margin-left: 0.5vw">Thêm</span>
                </p>
            </div>
            <div class="col-sm-7"></div>
            <div class="input-group col-sm-3">
                <input id="myInput" type="search" class="form-control rounded" placeholder="Tìm kiếm theo tên"
                    aria-label="Search" aria-describedby="search-addon" />
            </div>
        </div>
    </div>
    <div style="margin: 1vw">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">STT</th>
              <th scope="col">Mã SV</th>
              <th scope="col">Họ và tên</th>
              <th scope="col">Lớp</th>
              <th scope="col">Khoa</th>
              <th scope="col">Giới tính</th>
              <th scope="col">Ngày Sinh</th>
              <th scope="col">Quê quán</th>
              <th></th>
            </tr>
          </thead>
          <tbody id="listUser">

          </tbody>
        </table>
      </div>
</asp:Content>
